# envsecret

Tool to work with secrets (passwords/tokens) from the command line. Like a
simple password manager, but with the added functionality to create
environment variables. You can use it create and remove environment variables
with secrets or use command substitution to use secret tokens in commands.

## What the tool try to solve
You generally don't want to store passwords on disk in cleartext. You also
don't want to enter passwords manually every time you use them. And you dont
want passwords in your shell history file. So when you access stuff, which
requires passwords like databases or APIs, many people store their password in
an environment variable, so they can do stuff like `mysql -p $SQLPASSWORD`.
This is definately not a perfect solution, since the secret is available in the
process list and any admin on the machine can easily read the env-variables of
your process and find your passwords - BUT there is no perfect solution for
this. The secret needs to be send with the command.

## How the tool "solves" this
This tool makes it as easy and as fast as possible to create and remove
environment variables with your secrets. The tool can create a "safe", which is
an encrypted file, with all your passwords. When you need to use them, you can
"export" envsecrets, and it will then create the environment variables defined
in your safe (or the ones you ask for). When you are done, you can run 'envsecret
unset' to remove the secret environment variables. You can also use command
substitution, to replace a secret token in a command with a call to envsecret like
this:
```bash
mysql -p $(envsecret MYSQLPASS)
```
This means that the secret is only available as clear text in the process list
and never anywhere alse.

You will need to specify the password to you envsecret safe in stead of
your mysql password. Maybe not easier, but at least it is a local password for
a local file, so perhaps shorter and easier to remember and not exposed to
anyone.

This gives you "easier" access to your passwords/keys/tokens and it means, that
people can only "steal" your secrets while you are actually using them
(unless you leave shells running without deleting the environment variables).

You still need to remember and use the passphrase for the safe - but you don't
need more than this single one - much like when your using a password manager :-)

## Example
You can call envsecret -h to see the usage documentation.

Add a new password to a safe (ask to create safe if it doesn't exist)
```bash
envsecret add GITLABTOKEN
```

You will then be asked to enter the secret token, which is saved with the name
GITLABTOKEN. If you run
```bash
source envsecret export
or
source envsecret export GITLABTOKEN
```
The 1st command will export all variables in the safe to environment variables,
while the 2nd will only export GITLABTOKEN. You can then call the gitlab API
using this environment variable.  When you are done you can remove the variable
by running envsecret again:
```bash 
source envsecret unset
```
... or you can just use the shell unset command :-)

If you don't like having passwords saved in environment variables, you can also
just use command substitution as shown in the "how" section above.

## Installation and setup functions
Envsecret is just a single file bash shell-script, which can just be called
directly. So just copy the script to a suitable location.  If you wish to
create and remove environment variables, it needs to be sourced, so you don't
need to put the script in your path, since executing the script will run the
script in a new sub-shell and creating env variable in a subshell wont have any
effect in the original environment. So to use this functionality, you can
either source the script manually every time or use the setup action in the
script itself:
```bash
. path/envsecret setup
```
This will just create a bash alias "envsecret" which sources the script, so after
setup is run, you can just run `envsecret export`. If you want to make envsecret
available allways, you can just add
```
. path/envsecret quietsetup
```
to you .bashrc or whatever shell startup files you use. The only difference between
setup and quietsetup is that the former prints a status message, while the latter
don't output anything.

Using an alias will however prevent you from using envsecret in scripts, since alias'
are not inherited. So in scripts you have to source it manually and use the full path
to the script.

## Safe file
If you call envsecret, and there is no safe, envsecret will ask if it should create
one. By default envsecret uses the file ~/.envsecret, but it can be overwritte
using the -f option.

The safe file is just a text file, with a hash of the password, and all secrets
encrypted using symmetric encryption with gpg. If you haven't used gpg before
gpg will create a .gnupg subdirectory in your home folder the first time you 
use it.

## Usage
Call envsecret with the -h option to see the usage documentation.
Show which secrets are in the safe (end when they were created and any
comment/note associated with the secret)
  - envsecret list
Add a variable, APIKEY, to the safe prompting for secret value and a comment
  - envsecret add APIKEY
Same but specify comment directly in the command
  - envsecret add APIKEY "My API key for the great API @ www.coolapi.com"
Add a variable, APIKEY, to the safe specifying the secret in the command (watch your history file)
  - envsecret add APIKEY=ai45aniacnyt3478n
Remove a variable from safe
  - envsecret del APIKEY
Show secret value as statement (VAR=secret)
  - envsecret show VAR
Show just the secret value.
  - envsecret secret VAR 
  or
  - envsecret VAR
This can be used with command substitution:
  - curl -H "APIKEY:$(envsecret APIKEY)" http://great-api.com
Export all variables to environment variables
  - envsecret export
Export a single variable to environment
  - envsecret export VARNAME
Unset all the variables (you can also unset just a single variable, but it would be easier
just to skip envsecret, and just call unset yourself :-))
  - envsecret unset
Update the secret for a variable (can also be done with the = syntax)
  - envsecret update VAR
Delete all variables in the safe (you could also just delete the file)
  - envsecret purge
Change the safe password
  - envsecret passwd
Setup alias to souce envsecret file (use quietsetup if you don't want output)
  - . path/envsecret setup
Run the self-test function. This is just included unit-tests, which are nice to
have, when you make changes to the script.
  - envsecret test

### Options
You can modify the behavior using various options. 
- -h Show usage
- -p Specify the password to the safe (or -b if you specify a base64 encoded version of the password).
- -s
Store the supplied password in the env variable ENVSECRETPP. Envsecret will try the 
password stored in this variable, before it prompts for a password, so this will
ensure that you can run envsecret, without specifying the password every time. To
work, the script must be sourced (se above). It is not the clear text password
that is stored, it is base64 encoded. Not to be more secure, but to ensure that
strange characters wont ruin stuff and make it somewhat more difficult to
remember, if someone accidently sees it.
- -f Specify which file to use as safe
- -y Assume yes to all questions (careful)
- -q Quiet. Don't tell what is going on. Nice in scripts.
- -d Debug. Show function stack whenever a function is called. Efficient to learn how envsecret works.

## Subshell issues
If you run a script, it will run in a subshell, and any environment variables
you create will only live in this subshell. When you return, they will be gone.
Therefore this script needs to be sourced (using source or .)

But since the script defines a bunch of functions and variables, we don't want
to source the whole thing. This would polute your environment and potentially
overwrite some of your own stuff. So when you source envsecret, it will detect,
that it is beeing sourced, and it will then call itself creating a subshell.
Everything is then done in this subshell, and when we return from the subshell,
the environment variables will be created / removed based on output from the
subshell version. 

I couldn't figure out a way to do this without using a tmp-file, so the script
creates a tmp-file with the export and unset commands to manipulate env
variables, and the sourced script then sources the lines in this file. If you
call the script directly (not sourcing it) you can still do anything not related
to altering the environment, like adding/removing variables or creating new safes
or other stuff. But you can't create or remove environment variables. The commands
to do that, will simply be echoed to stdout.

## Contact
Envsecret is written by Kristian Hougaard (https://gitlab.com/ougar)
[@kghougaard](https://twitter.com/kghougaard) on Twitter. You can always grap
the latest version on Gitlab: (https://gitlab.com/ougar/envsecret). Feedback of
any kind is welcome. Also feedback like: Why the hell did you make this when Y
exists?  Because I would like to now about Y :-)
